# RecursiveBinarySearch

A Recursive Binary Search, a type of Divide and Conquer algorithm that searches for a number in a list of integers and returns the index or position of the number if found in the list.
This program accepts bot positive and negative integers which are sorted or in ascending order. Negative numbers can also be searched.
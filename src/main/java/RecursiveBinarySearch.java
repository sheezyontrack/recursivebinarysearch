/**
 * A Divide and Conquer Algorithm that accepts a sorted list of positive integers,
 * checks and compares the number been searched with the number at the position of the middle position.
 * It decreases the search space and reassigns the first or last index depending on the value of
 * the number been searched and the numbers in the list. It does this recursively until all the search space is covered
 */
public class RecursiveBinarySearch {
    // A method that takes in a list of integers, the number to search for, the first and last index of the numbers in the list
    public static int recursiveBinarySearch(int[] sortedPositiveIntegers, int searchKey, int firstIndex, int lastIndex) {

       // Declaration and initialization of the middle index number
       int middleIndex = firstIndex + (lastIndex - firstIndex)/2;

        // A nested IF that runs when the index of the last number is greater or equal to the index of the first number
       if (lastIndex >= firstIndex) {
           /**
            * Check for the number at the position of the middle Index and compare it to the number been searched,
            * return the index if the number is found at that position
            */
         if (searchKey == sortedPositiveIntegers[middleIndex]) {
               return middleIndex;
           }

           /**
            * If the searched number is smaller than the number located at the middle index,
            * decrease the search space by disregard all the numbers starting from
            * the current middle index up to the numbers at the last index and reassigning the position of the last index.
            */
           else if (searchKey < sortedPositiveIntegers[middleIndex]) {
               // A recursive method
               return recursiveBinarySearch(sortedPositiveIntegers, searchKey, firstIndex, middleIndex - 1);
           }

           /**
            * If the searched number is bigger than the number located at the position of the current middle index,
            * decrease the search space, as it is a sorted list of integers (in ascending order)
            * re-assign the last index to the number located one step above the middle index number
            */
           else {
               return recursiveBinarySearch(sortedPositiveIntegers, searchKey, middleIndex + 1, lastIndex);
           }
       }

       // At this point, no number was found in the list
       return -1;
    }

    // The main method to test the program
    public static void main(String[] args) {
        // Sorted list of integers
        int[] sortedList = {-20, -9, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int searchKey = -9; // The number that we are searching for

        // Declaration and initialization of the position or index obtained from the recursive method
        int currentIndex = recursiveBinarySearch(sortedList, searchKey,   0, sortedList.length -1);

        // If the index is not a negative integer, then it must have been found or contained in the list
        if (currentIndex != -1) {
            System.out.println(" ");
            System.out.println("The number " + searchKey + " was found at position " + currentIndex);
        } else {
            // What to print out when the number is not contained in the list
            System.out.println(" ");
            System.out.println("The number " + searchKey + " was not found in the list");
        }
    }
}
